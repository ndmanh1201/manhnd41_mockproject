import com.android.build.api.dsl.ApplicationExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

class AndroidApplicationConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("com.android.application")
                apply("org.jetbrains.kotlin.android")
            }
            extensions.configure<ApplicationExtension> {
                compileSdk = 34
                defaultConfig.apply {
                    applicationId = "com.example.mockprojectnewsapp"
                    minSdk = 24
                    targetSdk = 34
                    versionCode = 1
                    versionName = "1.0.0"

                    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
                    vectorDrawables {
                        useSupportLibrary = true
                    }
                    buildConfigField("String", "BASE_URL", "\"https://newsapi.org/v2/\"")
                    buildConfigField("String", "API_KEY", "\"07d6b75bbd4f46d2a01f9ca36b646299\"")
                }
                compileOptions {
                    sourceCompatibility = JavaVersion.VERSION_17
                    targetCompatibility = JavaVersion.VERSION_17
                }
                buildFeatures.apply {
                    buildConfig = true
                    viewBinding = true
                }
                buildToolsVersion = "34.0.0"
            }
        }
    }
}