package com.example.mockprojectnewsapp.ui.component.home


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mockprojectnewsapp.TestDispatcher
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsByCategoryUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsInTheUSUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.SaveToDatabaseUseCase
import com.example.mockprojectnewsapp.utils.NetworkConfig
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.test.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var getNewsInTheUSUseCase: GetNewsInTheUSUseCase

    @MockK
    lateinit var getNewsByCategoryUseCase: GetNewsByCategoryUseCase

    @MockK
    lateinit var saveToDatabaseUseCase: SaveToDatabaseUseCase

    @MockK
    lateinit var homeViewModel: HomeViewModel
    lateinit var testDispatcher: TestDispatcher

    @MockK
    lateinit var networkConfig: NetworkConfig

    @Before
    fun setUp() {
        getNewsInTheUSUseCase = mockk()
        getNewsByCategoryUseCase = mockk()
        saveToDatabaseUseCase = mockk()
        networkConfig = mockk()
        testDispatcher = TestDispatcher()

        homeViewModel =
            HomeViewModel(
                getNewsInTheUSUseCase,
                getNewsByCategoryUseCase,
                saveToDatabaseUseCase,
                testDispatcher
            )

    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun givenNoInternetConnection_whenCallGetMainData_thenReturnArticleList() = runTest {

        every { getNewsInTheUSUseCase.fromLocal() } returns flow {
            emit(
                listOf(
                    Article(
                        title = null,
                        author = null,
                        description = null,
                        content = null,
                        publishedAt = null,
                        urlToImage = null
                    )
                )
            )

            every { getNewsByCategoryUseCase.fromLocal("Technology") } returns flow {
                emit(
                    listOf(
                        Article(
                            title = null,
                            author = null,
                            description = null,
                            content = null,
                            publishedAt = null,
                            urlToImage = null
                        )
                    )
                )



                homeViewModel.getMainData()
                val latestNewsList = homeViewModel.latestUSNewsList.value
                val newsByCategoryList = homeViewModel.newsByCategoryList.value
                assertThat(latestNewsList?.size).isEqualTo(1)
                assertThat(newsByCategoryList?.size).isEqualTo(1)
            }
        }
    }
}