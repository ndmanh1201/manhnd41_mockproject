package com.example.mockprojectnewsapp.ui.component.signup

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.example.mockprojectnewsapp.TestDispatcher
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.usecase.user.RegisterUseCase
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SignUpViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var registerUseCase: RegisterUseCase

    @MockK
    lateinit var signUpViewModel: SignUpViewModel

    lateinit var testDispatcher: TestDispatcher

    @Before
    fun setUp() {
        registerUseCase = mockk()
        testDispatcher = TestDispatcher()
        signUpViewModel = SignUpViewModel(registerUseCase, testDispatcher)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun givenUser_whenCallRegister_thenReturnFirebaseStateSuccess() = runTest {
        val stateSuccess = FireBaseState.Success("")
        val user = User(
            email = "test@gmail.com",
            password = "manhmanh"
        )
        coEvery { registerUseCase(user) } returns stateSuccess
        signUpViewModel.registerState.test {
            signUpViewModel.register(user)
            Truth.assertThat(awaitItem()).isEqualTo(stateSuccess)
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun givenUser_whenCallRegister_thenReturnFirebaseStateFail() = runTest {

        val stateFail = FireBaseState.Fail<String>("")
        val user = User(
            email = "test@gmail.com",
            password = "manhmanh"
        )
        coEvery { registerUseCase(user) } returns stateFail
        signUpViewModel.registerState.test {
            signUpViewModel.register(user)
            Truth.assertThat(awaitItem()).isEqualTo(stateFail)
        }
    }
}