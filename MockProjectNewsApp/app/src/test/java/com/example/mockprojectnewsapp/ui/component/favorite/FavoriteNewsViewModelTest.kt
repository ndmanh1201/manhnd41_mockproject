package com.example.mockprojectnewsapp.ui.component.favorite

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mockprojectnewsapp.TestDispatcher
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.usecase.favorite.AddToFavoriteUseCase
import com.example.mockprojectnewsapp.domain.usecase.favorite.GetFavoriteNewUseCase
import com.google.common.truth.Truth.assertThat
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FavoriteNewsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var getFavoriteNewUseCase: GetFavoriteNewUseCase

    @MockK
    lateinit var addToFavoriteUseCase: AddToFavoriteUseCase

    lateinit var testDispatcher: TestDispatcher

    @MockK
    lateinit var favoriteNewsViewModel: FavoriteNewsViewModel


    @Before
    fun setUp() {
        getFavoriteNewUseCase = mockk()
        addToFavoriteUseCase = mockk()
        testDispatcher = TestDispatcher()
        favoriteNewsViewModel = FavoriteNewsViewModel(getFavoriteNewUseCase, addToFavoriteUseCase, testDispatcher)
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun whenCallGetFavoriteNews_thenReturnNewsList() = runTest {

        val list = listOf(Article(
            title = null,
            author = null,
            description = null,
            content = null,
            publishedAt = null,
            urlToImage = null
        ))

        every { getFavoriteNewUseCase() } coAnswers { flow { emit(list) } }
        favoriteNewsViewModel.getFavoriteNews()
        val result = favoriteNewsViewModel.favoriteNewsList.value
        assertThat(result).isEqualTo(list)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun givenArticle_whenAddToFavorite_returnAddToFavoriteState() = runTest {
        val article = Article(
            title = null,
            author = null,
            description = null,
            content = null,
            publishedAt = null,
            urlToImage = null
        )
        val stateSuccess = FireBaseState.Success("") as FireBaseState<String>
        coEvery { addToFavoriteUseCase(article) } returns stateSuccess
        favoriteNewsViewModel.addToFavorite(article)
        assertThat(favoriteNewsViewModel.addToFavoriteState.value).isEqualTo(stateSuccess)
    }

}