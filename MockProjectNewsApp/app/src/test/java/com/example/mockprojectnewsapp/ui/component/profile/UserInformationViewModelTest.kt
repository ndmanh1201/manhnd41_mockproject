package com.example.mockprojectnewsapp.ui.component.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.example.mockprojectnewsapp.TestDispatcher
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.usecase.user.GetUserInfoUseCase
import com.example.mockprojectnewsapp.domain.usecase.user.UserProfileUpdateUseCase
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UserInformationViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var getUserInfoUseCase: GetUserInfoUseCase

    @MockK
    lateinit var userProfileUpdateUseCase: UserProfileUpdateUseCase

    @MockK
    lateinit var userInformationViewModel: UserInformationViewModel

    lateinit var testDispatcher: TestDispatcher

    @Before
    fun setUp() {
        getUserInfoUseCase = mockk()
        userProfileUpdateUseCase = mockk()
        testDispatcher = TestDispatcher()
        userInformationViewModel = UserInformationViewModel(
            getUserInfoUseCase,
            userProfileUpdateUseCase,
            testDispatcher
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun whenCallGetUserInfo_thenReturnUserInfo() = runTest {
        val user = User(
            email = "test@gmail.com",
            password = "12345678",
            username = "test",
            firstName = "manh",
            lastName = "nguyen"
        )
        every { getUserInfoUseCase() } returns flow { emit(user) }

        userInformationViewModel.getUserInfo()
        Truth.assertThat(userInformationViewModel.userInfo.value).isEqualTo(user)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun givenUserAndUpdateInformation_whenCallGetUserInfo_thenFirebaseStateSuccess() = runTest {
        val user = User(
            email = "test@gmail.com",
            password = "12345678",
            username = "test",
            firstName = "manh",
            lastName = "nguyen"
        )

        var newUserInformation = hashMapOf<String, Any?>()
        user.let {
            newUserInformation = hashMapOf(
                "/firstName" to "test",
                "/lastName" to "test"
            )
        }

        val stateSuccess = FireBaseState.Success<String>(null)

        coEvery { userProfileUpdateUseCase(user, newUserInformation) } returns stateSuccess

        userInformationViewModel.updateProfileState.test {
            userInformationViewModel.updateUserProfile(user, newUserInformation)
            Truth.assertThat(awaitItem()).isEqualTo(stateSuccess)
        }
    }
}