package com.example.mockprojectnewsapp.ui.component.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.example.mockprojectnewsapp.TestDispatcher
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.usecase.user.LoginUseCase
import com.example.mockprojectnewsapp.domain.usecase.user.LogoutUseCase
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SignInViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @MockK
    lateinit var loginUseCase: LoginUseCase

    @MockK
    lateinit var logoutUseCase: LogoutUseCase

    @MockK
    lateinit var signInViewModel: SignInViewModel

    lateinit var testDispatcher: TestDispatcher

    @Before
    fun setUp() {
        loginUseCase = mockk()
        logoutUseCase = mockk()
        testDispatcher = TestDispatcher()
        signInViewModel = SignInViewModel(loginUseCase, logoutUseCase, testDispatcher)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun givenEmailPassword_whenCallLogin_thenReturnSuccess() = runTest {
        val loginState = FireBaseState.Success("") as FireBaseState<String>
        coEvery { loginUseCase("manh@gmail.com", "12345678") } returns loginState
        signInViewModel.loginState.test {
            signInViewModel.login("manh@gmail.com", "12345678")
            val result = awaitItem()
            Truth.assertThat(result).isEqualTo(loginState)
            cancelAndConsumeRemainingEvents()
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun whenCallLogout_thenReturnFirebaseStateFail() = runTest {
        signInViewModel.loginState.test {
            signInViewModel.logout()
            val expect = when(awaitItem()) {
                is FireBaseState.Fail -> true
                is FireBaseState.Success -> false
            }
            Truth.assertThat(expect).isTrue()
        }
    }
}