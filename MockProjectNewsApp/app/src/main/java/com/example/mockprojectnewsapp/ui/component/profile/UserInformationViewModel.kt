package com.example.mockprojectnewsapp.ui.component.profile

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.usecase.user.GetUserInfoUseCase
import com.example.mockprojectnewsapp.domain.usecase.user.UserProfileUpdateUseCase
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserInformationViewModel @Inject constructor(
    private val getUserInfoUseCase: GetUserInfoUseCase,
    private val userProfileUpdateUseCase: UserProfileUpdateUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _userInfo = MutableLiveData<User>()
    val userInfo: LiveData<User> get() = _userInfo

    private val _updateProfileState = MutableSharedFlow<FireBaseState<String>>()
    val updateProfileState: SharedFlow<FireBaseState<String>> get() = _updateProfileState

    private val _userImageUri = MutableLiveData<Uri>()
    val userImageUri: LiveData<Uri> get() = _userImageUri

    fun getUserInfo() {
        viewModelScope.launch(dispatcher.main) {
            getUserInfoUseCase().collect {
                _userInfo.value = it
            }
        }
    }

    fun updateUserProfile(user: User?, updatedInformation: HashMap<String, Any?>) {
        viewModelScope.launch(dispatcher.io) {
            _updateProfileState.emit(
                userProfileUpdateUseCase(user, updatedInformation)
            )
        }
    }

    fun getUserImage(uri: Uri) {
        viewModelScope.launch {
            _userImageUri.value = uri
        }
    }

}