package com.example.mockprojectnewsapp.ui.component.favorite

import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mockprojectnewsapp.databinding.FragmentFavoriteBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoriteFragment : BaseFragment<FragmentFavoriteBinding>(FragmentFavoriteBinding::inflate) {

    @Inject lateinit var favoriteNewsAdapter: FavoriteNewsAdapter
    private val favoriteNewsViewModel: FavoriteNewsViewModel by viewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        favoriteNewsViewModel.favoriteNewsList.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.progressView.isVisible = true
            } else {
                binding.progressView.isVisible = false
                updateUI(it)
            }
        }
    }

    private fun updateUI(articleList: List<Article>) {
        favoriteNewsAdapter.submitList(articleList)
        favoriteNewsAdapter.setOnItemClickListener {
            val action = FavoriteFragmentDirections.actionFavoriteFragmentToDetailFragment(it)
            findNavController().navigate(action)
        }
        binding.recyclerFavoriteNews.apply {
            adapter = favoriteNewsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    override fun bindView() {
        super.bindView()
        favoriteNewsViewModel.getFavoriteNews()

        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }


}