package com.example.mockprojectnewsapp.ui.component.splash

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.mockprojectnewsapp.databinding.FragmentSplashBinding
import com.example.mockprojectnewsapp.utils.extensions.safeNavigate
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            delay(1700)
            withContext(Dispatchers.Main) {
                findNavController().safeNavigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment())
            }
        }
    }



}