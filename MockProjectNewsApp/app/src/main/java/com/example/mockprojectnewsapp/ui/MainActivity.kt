package com.example.mockprojectnewsapp.ui


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.mockprojectnewsapp.R
import com.example.mockprojectnewsapp.databinding.ActivityMainBinding
import com.example.mockprojectnewsapp.ui.component.home.HomeFragment
import com.example.mockprojectnewsapp.utils.NetworkConfig
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var backPressedTime: Long = 0
    lateinit var backToast: Toast

    @SuppressLint("SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpBottomNav()

        CoroutineScope(Dispatchers.Default).launch {
            NetworkConfig.checkNetworkAvailable(this@MainActivity)
        }
        pressAgainToExit()
    }

    private fun setUpBottomNav() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_container_view) as NavHostFragment
        val navController = navHostFragment.navController

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.homeFragment -> showBottomNav()
                R.id.profileFragment -> showBottomNav()
                R.id.favoriteFragment -> showBottomNav()
                else -> hideBottomNav()
            }
        }


        binding.bottomNavigationBar.apply {
            setupWithNavController(navController)
            setOnItemReselectedListener { item ->
                val reSelectedDestinationId = item.itemId
                navController.popBackStack(reSelectedDestinationId, inclusive = false)
            }
        }
    }

    private fun showBottomNav() {
        binding.bottomNavigationBar.visibility = View.VISIBLE
    }

    private fun hideBottomNav() {
        binding.bottomNavigationBar.visibility = View.GONE
    }

    private fun pressAgainToExit() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.homeFragment)
        if (currentFragment is HomeFragment) {
            val callBack = onBackPressedDispatcher.addCallback( object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    backToast = Toast.makeText(this@MainActivity, "Press back again to leave the app.", Toast.LENGTH_LONG)
                    if (backPressedTime + 2000 > System.currentTimeMillis()) {
                        backToast.cancel()
                        return
                    } else {
                        backToast.show()
                    }
                    backPressedTime = System.currentTimeMillis()
                }
            })
        }
    }


}