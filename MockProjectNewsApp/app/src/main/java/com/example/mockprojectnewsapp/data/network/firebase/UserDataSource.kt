package com.example.mockprojectnewsapp.data.network.firebase

import com.example.mockprojectnewsapp.data.model.UserFirebaseModel
import kotlinx.coroutines.flow.Flow

interface UserDataSource {
    suspend fun login(email: String, password: String): Boolean
    suspend fun signup(userFirebaseModel: UserFirebaseModel): Boolean
    fun getUserInfo(): Flow<UserFirebaseModel>
    fun logout()

    suspend fun updateUserProfile(userFirebaseModel: UserFirebaseModel?, updatedInformation: HashMap<String, Any?>): Boolean
}