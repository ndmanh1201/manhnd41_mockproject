package com.example.mockprojectnewsapp.ui.component.profile

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.mockprojectnewsapp.R
import com.example.mockprojectnewsapp.databinding.FragmentProfileBinding
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import com.example.mockprojectnewsapp.ui.component.login.SignInViewModel
import com.example.mockprojectnewsapp.utils.LoginConfig
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private val userInfoViewModel: UserInformationViewModel by activityViewModels()
    private val signInViewModel: SignInViewModel by activityViewModels()

    @Inject lateinit var imageLoader: ImageLoader

    override fun observeViewModel() {
        super.observeViewModel()
        userInfoViewModel.userInfo.observe(viewLifecycleOwner) {
            updateUI(it)
        }
    }

    override fun bindView() {
        super.bindView()
        binding.viewMyAccount.setOnClickListener {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditProfileFragment())
        }

        binding.view.setOnClickListener {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToEditProfileFragment())
        }
        binding.viewLogOut.setOnClickListener {
            LoginConfig.loginState(requireContext(), "logged_out")
            signInViewModel.logout()
            findNavController().popBackStack(R.id.loginFragment, false)
        }

        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun updateUI(user: User) {
        binding.apply {
            tvUsername.text = user.username
            tvEmail.text = user.email
            if (user.imageUrl != null) {
                imageLoader.load(imgUserImage, user.imageUrl!!)
            }
        }
    }
}