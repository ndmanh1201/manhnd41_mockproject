package com.example.mockprojectnewsapp.data.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class UserFirebaseModel(
    var uid: String = "",
    var email: String = "",
    var username: String = "",
    var password: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var dateOfBirth: String = "",
    var imageUrl: String = ""
)