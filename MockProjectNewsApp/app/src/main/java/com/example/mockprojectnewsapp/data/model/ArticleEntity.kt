package com.example.mockprojectnewsapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Article")
data class ArticleEntity(

    @PrimaryKey
    @SerializedName("title")
    @ColumnInfo("title") var title: String,

    @SerializedName("author")
    @ColumnInfo("author") var author: String?,

    @SerializedName("description")
    @ColumnInfo("description") var description: String?,

    @SerializedName("content")
    @ColumnInfo("content") var content: String?,

    @SerializedName("publishedAt")
    @ColumnInfo("publishedAt") var publishedAt: String?,

    @SerializedName("urlToImage")
    @ColumnInfo("urlToImage") var urlToImage: String?,

    @Transient
    @ColumnInfo("category") var category: String?

) {
    constructor() : this( "", null, null, null, null, null, null)
}