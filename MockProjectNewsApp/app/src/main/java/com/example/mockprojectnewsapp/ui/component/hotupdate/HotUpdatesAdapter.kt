package com.example.mockprojectnewsapp.ui.component.hotupdate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.mockprojectnewsapp.databinding.ItemHotUpdatesBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.ui.base.BaseAdapter
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import javax.inject.Inject

class HotUpdatesAdapter @Inject constructor(
    private val imageLoader: ImageLoader
) : BaseAdapter<ItemHotUpdatesBinding, Article>() {
    override fun bindViewHolder(binding: ItemHotUpdatesBinding, item: Article) {
        binding.apply {
            item.also {
                tvItemTitle.text = it.title
                tvItemDescription.text = it.description
                tvItemAuthor.text = it.author

                imageLoader.load(imgItemImage, it.urlToImage!!)
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemHotUpdatesBinding
        get() = { inflater, parent, _ ->
            ItemHotUpdatesBinding.inflate(inflater, parent, false)
        }
}