package com.example.mockprojectnewsapp.data.mapper

import com.example.mockprojectnewsapp.data.model.ArticleEntity
import com.example.mockprojectnewsapp.data.model.NewsFirebaseModel
import com.example.mockprojectnewsapp.domain.model.Article

object NewsMapper {

    fun mapFromEntity(source: List<ArticleEntity>): List<Article> {
        return source.map {
            Article(
                title = it.title,
                author = it.author ?: "",
                description = it.description ?: "",
                content = it.content ?: "",
                publishedAt = it.publishedAt ?: "",
                urlToImage = it.urlToImage ?: ""
            )
        }
    }

    fun mapFromFirebaseModel(source: List<NewsFirebaseModel>): List<Article> {
        return source.map {
            Article(
                title = it.title,
                author = it.author,
                description = it.description,
                content = it.content,
                publishedAt = it.publishedAt,
                urlToImage = it.urlToImage
            )
        }
    }

    fun mapFromModel(source: List<Article>, category: String): List<ArticleEntity> {
        return source.map {
            ArticleEntity().apply {
                title = it.title!!
                author = it.author!!
                description = it.description!!
                content = it.content!!
                publishedAt = it.publishedAt!!
                urlToImage = it.urlToImage!!
                this.category = category
            }
        }
    }

    fun mapFromModelToFirebaseModel(source: Article): NewsFirebaseModel {
        return NewsFirebaseModel().apply {
            this.title = source.title!!
            this.author = source.author!!
            this.description = source.description!!
            this.content = source.content!!
            this.publishedAt = source.publishedAt!!
            this.urlToImage = source.urlToImage!!
        }

    }

}