package com.example.mockprojectnewsapp.di

import android.content.Context
import androidx.room.Room
import com.example.mockprojectnewsapp.data.local.NewsDao
import com.example.mockprojectnewsapp.data.local.NewsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {

    @Provides
    @Singleton
    fun provideNewsDao(newsDatabase: NewsDatabase) : NewsDao {
        return newsDatabase.newsDao
    }

    @Provides
    @Singleton
    fun provideNewsDatabase(@ApplicationContext applicationContext: Context): NewsDatabase {
        return Room.databaseBuilder(
            applicationContext,
            NewsDatabase::class.java,
            "NewsDB"
        ).build()
    }

}