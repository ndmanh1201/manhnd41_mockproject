package com.example.mockprojectnewsapp.ui.component.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.usecase.favorite.AddToFavoriteUseCase
import com.example.mockprojectnewsapp.domain.usecase.favorite.GetFavoriteNewUseCase
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteNewsViewModel @Inject constructor(
    private val getFavoriteNewUseCase: GetFavoriteNewUseCase,
    private val addToFavoriteUseCase: AddToFavoriteUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _favoriteNewsList = MutableLiveData<List<Article>>()
    val favoriteNewsList: LiveData<List<Article>> get() = _favoriteNewsList

    private val _addToFavoriteState = MutableLiveData<FireBaseState<String>?>()
    val addToFavoriteState: LiveData<FireBaseState<String>?> get() = _addToFavoriteState

    fun getFavoriteNews() {
        viewModelScope.launch(dispatcher.main) {
            getFavoriteNewUseCase().collect {
                _favoriteNewsList.value = it
            }
        }
    }

    fun addToFavorite(article: Article) {
        viewModelScope.launch(dispatcher.main) {
            _addToFavoriteState.value = addToFavoriteUseCase(article)

        }
    }

}