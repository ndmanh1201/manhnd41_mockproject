package com.example.mockprojectnewsapp.ui.component.viewpager

import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mockprojectnewsapp.databinding.FragmentNewsByCategoryBinding
import com.example.mockprojectnewsapp.ui.component.home.NewsByCategoryAdapter
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import com.example.mockprojectnewsapp.ui.component.home.HomeFragmentDirections
import com.example.mockprojectnewsapp.ui.component.home.HomeViewModel
import com.example.mockprojectnewsapp.utils.NetworkConfig
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TechnologyFragment : BaseFragment<FragmentNewsByCategoryBinding>(FragmentNewsByCategoryBinding::inflate){
    private val homeViewModel: HomeViewModel by activityViewModels()
    @Inject lateinit var newsByCategoryAdapter: NewsByCategoryAdapter

    override fun observeViewModel() {
        super.observeViewModel()
        homeViewModel.newsByCategoryList.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.progressBar.isVisible = true
            } else {
                binding.progressBar.isVisible = false
                newsByCategoryAdapter.submitList(it)
                setUpRecyclerView()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        NetworkConfig.unregisterNetworkManager(requireContext())
    }

    private fun setUpRecyclerView() {
        binding.recyclerViewNewsByCategory.apply {
            adapter = newsByCategoryAdapter
            layoutManager = LinearLayoutManager(requireContext())
            newsByCategoryAdapter.setOnItemClickListener {
                val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(it)
                findNavController().navigate(action)
            }
        }
    }
}