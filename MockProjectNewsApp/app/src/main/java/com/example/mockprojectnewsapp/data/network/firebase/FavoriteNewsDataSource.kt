package com.example.mockprojectnewsapp.data.network.firebase

import com.example.mockprojectnewsapp.data.model.NewsFirebaseModel
import kotlinx.coroutines.flow.Flow

interface FavoriteNewsDataSource {
    suspend fun addToFavorite(newsFirebaseModel: NewsFirebaseModel) : Boolean
    fun getFavoriteNews(): Flow<List<NewsFirebaseModel>>

    suspend fun removeFavorite(newsFirebaseModel: NewsFirebaseModel) : Boolean
}