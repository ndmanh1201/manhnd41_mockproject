package com.example.mockprojectnewsapp.ui.component.home

import android.os.Bundle
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mockprojectnewsapp.R
import com.example.mockprojectnewsapp.databinding.FragmentHomeBinding
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import com.example.mockprojectnewsapp.ui.component.profile.UserInformationViewModel
import com.example.mockprojectnewsapp.utils.NetworkConfig
import com.example.mockprojectnewsapp.utils.TabLayoutConfig
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    @Inject
    lateinit var latestNewsAdapter: LatestNewsAdapter
    @Inject
    lateinit var imageLoader: ImageLoader

    private val homeViewModel: HomeViewModel by activityViewModels()
    private val userInfoViewModel: UserInformationViewModel by activityViewModels()

    private var backPressedTime: Long = 0
    lateinit var backToast: Toast


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            //get main data
            homeViewModel.getMainData()

            //get user information
            userInfoViewModel.getUserInfo()
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        homeViewModel.latestUSNewsList.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.progressBar.isVisible = true
            } else {
                binding.progressBar.isVisible = false
                latestNewsAdapter.submitList(it)
                setUpLatestNewsAdapter()
            }
        }

        userInfoViewModel.userInfo.observe(viewLifecycleOwner) {
            imageLoader.load(binding.imgUserImage, it.imageUrl!!)
        }

    }

    override fun bindView() {
        super.bindView()

        pressAgainToExit()

        //config TabLayout
        TabLayoutConfig.apply {
            setUpTabLayout(binding.tabLayoutCategory, requireContext())
            setUpViewPager(binding.viewPagerNewsByCategory, this@HomeFragment)
            setUpTabLayoutWithViewPager(
                binding.tabLayoutCategory,
                binding.viewPagerNewsByCategory
            )
        }

        binding.tvSeeAll.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_hotUpdateFragment)
        }

        binding.edtSearch.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_searchFragment)
        }


    }

    private fun setUpLatestNewsAdapter() {
        binding.recyclerViewLatestNews.apply {
            adapter = latestNewsAdapter
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
            latestNewsAdapter.setOnItemClickListener {
                val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(it)
                findNavController().navigate(action)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        NetworkConfig.unregisterNetworkManager(requireContext())
    }

    private fun pressAgainToExit() {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                backToast = Toast.makeText(
                    requireContext(),
                    "Press back again to leave the app.",
                    Toast.LENGTH_LONG
                )
                if (backPressedTime + 2000 > System.currentTimeMillis()) {
                    backToast.cancel()
                    requireActivity().finish()
                    return
                } else {
                    backToast.show()
                }
                backPressedTime = System.currentTimeMillis()
            }
        })
    }


}