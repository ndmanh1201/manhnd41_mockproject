package com.example.mockprojectnewsapp.data.network.firebase

import android.util.Log
import androidx.core.net.toUri
import com.example.mockprojectnewsapp.data.model.UserFirebaseModel
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await

class UserDataSourceImpl(
    private val auth: FirebaseAuth,
    private val databaseRef: DatabaseReference,
    private val storageReference: StorageReference
) : UserDataSource{

    override suspend fun login(email: String, password: String) : Boolean {
        return try {
            auth.signInWithEmailAndPassword(email, password).await()
            true
        } catch (ex: FirebaseException) {
            false
        }
    }

    override suspend fun signup(userFirebaseModel: UserFirebaseModel): Boolean {
        return try {
            auth.createUserWithEmailAndPassword(userFirebaseModel.email, userFirebaseModel.password).await()
            val currentUser = auth.currentUser
            currentUser?.let {
                databaseRef.child("User").child(it.uid).setValue(userFirebaseModel.apply { uid = it.uid })
            }
            true
        } catch (ex: FirebaseException) {
            false
        }
    }

    override fun getUserInfo(): Flow<UserFirebaseModel> {
        val userRef = databaseRef.child("User").child(auth.currentUser!!.uid)
        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val user = snapshot.getValue(UserFirebaseModel::class.java)
                    user?.let {
                        trySend(it)
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
            }

            userRef.addValueEventListener(valueEventListener)
            awaitClose {userRef.removeEventListener(valueEventListener)}
        }
    }

    override fun logout() {
        auth.signOut()
    }

    override suspend fun updateUserProfile(userFirebaseModel: UserFirebaseModel?, updatedInformation: HashMap<String, Any?>): Boolean {
        return try {
            if (userFirebaseModel != null) {
                userFirebaseModel.uid = auth.currentUser!!.uid
                val userRef = databaseRef.child("User").child(userFirebaseModel.uid)
                val storageRef =  storageReference.child(userFirebaseModel.uid)
                storageRef.putFile(userFirebaseModel.imageUrl.toUri()).continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    storageRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result
                        updatedInformation["/imageUrl"] = downloadUri.toString()
                        userRef.updateChildren(updatedInformation)
                        Log.d("manh", "updateUserProfile at line 89: $updatedInformation")
                    }
                }
                true
            } else {
                false
            }
        } catch (ex: FirebaseException) {
            false
        }
    }


}
