package com.example.mockprojectnewsapp.ui.component.search

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.mockprojectnewsapp.databinding.ItemSearchResultBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.ui.base.BaseAdapter
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import javax.inject.Inject

class SearchAdapter @Inject constructor(
    private val imageLoader: ImageLoader
) : BaseAdapter<ItemSearchResultBinding, Article>() {
    override fun bindViewHolder(binding: ItemSearchResultBinding, item: Article) {
        binding.apply {
            item.also {
                tvResultAuthor.text = it.author
                tvResultDescription.text = it.description
                tvResultTitle.text = it.title

                imageLoader.load(imgResultImage, it.urlToImage!!)

            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemSearchResultBinding
        get() = { inflater, parent, _ ->
            ItemSearchResultBinding.inflate(inflater, parent, false)
        }
}