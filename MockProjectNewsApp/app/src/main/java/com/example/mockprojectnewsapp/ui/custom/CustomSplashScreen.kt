package com.example.mockprojectnewsapp.ui.custom

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.mockprojectnewsapp.R

class CustomSplashScreen(context: Context, attrs: AttributeSet?) : View(context, attrs) {

    private val paintIcon: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var progress = 0F

    private var width: Int = 0
    private var height: Int = 0


    @Volatile
    private var paintAnimator: ValueAnimator = ValueAnimator.ofFloat(0F, 1F)

    init {
        paintAnimator.apply {
            interpolator = LinearInterpolator()
            duration = 1700
            addUpdateListener {
                progress = it.animatedValue as Float
                invalidate()
                requestLayout()
            }
            start()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        width = MeasureSpec.getSize(widthMeasureSpec)
        height = MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(width, height)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val iconBitmap =
            ResourcesCompat.getDrawable(resources, R.drawable.mock_pj_icon, null)!!
                .toBitmap(width, height)
        paintIcon.alpha = (progress * 255).toInt()
        canvas.drawBitmap(
            iconBitmap,
            0F,
            0F,
            paintIcon
        )
    }
}