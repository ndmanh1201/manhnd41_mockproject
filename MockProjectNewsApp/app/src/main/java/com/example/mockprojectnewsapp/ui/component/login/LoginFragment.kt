package com.example.mockprojectnewsapp.ui.component.login

import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.mockprojectnewsapp.databinding.FragmentSignInBinding
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.utils.extensions.safeNavigate
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import com.example.mockprojectnewsapp.utils.LoginConfig
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentSignInBinding>(FragmentSignInBinding::inflate) {

    private val signInViewModel: SignInViewModel by activityViewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            signInViewModel.loginState.collect {
                handleLoginState(it)
            }
        }
    }

    override fun bindView() {
        super.bindView()
        //auto login
        if (LoginConfig.isLogin(requireContext(), "logged_in")) {
            findNavController().safeNavigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
        }

        binding.apply {
            tvSignUp.setOnClickListener {
                findNavController().safeNavigate(LoginFragmentDirections.actionLoginFragmentToSignUpFragment())
            }

            btnLogin.setOnClickListener {
                binding.progressBar.isVisible = true
                signInViewModel.login(
                    edtLoginEmail.text.toString(),
                    edtLoginPassword.text.toString()
                )
            }
        }
    }


    private fun handleLoginState(fireBaseState: FireBaseState<String>) {
        when (fireBaseState) {
            is FireBaseState.Success -> {
                binding.progressBar.isVisible = false
                LoginConfig.loginState(requireContext(), "logged_in")
                findNavController().safeNavigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
            }

            is FireBaseState.Fail -> {
                binding.progressBar.isVisible = false
                if (!fireBaseState.msg.isNullOrEmpty()) {
                    Toast.makeText(requireContext(), fireBaseState.msg.toString(), Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
}