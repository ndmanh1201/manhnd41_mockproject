package com.example.mockprojectnewsapp.di

import com.app.news.core.data.network.interceptor.AppInterceptor
import com.example.mockprojectnewsapp.BuildConfig
import com.example.mockprojectnewsapp.data.network.api.ApiService
import com.example.mockprojectnewsapp.data.network.firebase.FavoriteNewsDataSource
import com.example.mockprojectnewsapp.data.network.firebase.FavoriteNewsDataSourceImpl
import com.example.mockprojectnewsapp.data.network.firebase.UserDataSource
import com.example.mockprojectnewsapp.data.network.firebase.UserDataSourceImpl
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.excludeFieldsWithoutExposeAnnotation()
        return gsonBuilder.create()
    }

    @Singleton
    @Provides
    fun providesHttpLoggingInterceptor() = HttpLoggingInterceptor()
        .apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Singleton
    @Provides
    fun providesAppInterceptor() = AppInterceptor(BuildConfig.API_KEY)

    @Singleton
    @Provides
    fun providesOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        appInterceptor: AppInterceptor
    ): OkHttpClient =
        OkHttpClient
            .Builder()
            .addInterceptor(appInterceptor)
            .addInterceptor(httpLoggingInterceptor)
            .build()


    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) : ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideFirebaseAuth() : FirebaseAuth {
        return Firebase.auth
    }

    @Provides
    @Singleton
    fun provideDatabaseReference() : DatabaseReference {
        return Firebase.database.reference
    }

    @Provides
    @Singleton
    fun provideFirebaseStorage() : StorageReference {
        return Firebase.storage.reference
    }

    @Provides
    @Singleton
    fun provideUserDataSource(auth: FirebaseAuth, databaseReference: DatabaseReference, storageReference: StorageReference) : UserDataSource {
        return UserDataSourceImpl(auth, databaseReference, storageReference)
    }

    @Provides
    @Singleton
    fun provideFavoriteNewsDataSource(auth: FirebaseAuth, databaseReference: DatabaseReference) : FavoriteNewsDataSource {
        return FavoriteNewsDataSourceImpl(auth, databaseReference)
    }
}