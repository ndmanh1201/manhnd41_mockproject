package com.example.mockprojectnewsapp.data.repository

import com.example.mockprojectnewsapp.data.mapper.NewsMapper
import com.example.mockprojectnewsapp.data.mapper.UserMapper
import com.example.mockprojectnewsapp.data.network.firebase.FavoriteNewsDataSource
import com.example.mockprojectnewsapp.data.network.firebase.UserDataSource
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.repository.UserRepository
import com.example.mockprojectnewsapp.utils.InputFieldValidator
import com.google.firebase.FirebaseException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserRepositoryImpl(
    private val userDataSource: UserDataSource,
    private val favoriteNewsDataSource: FavoriteNewsDataSource
) : UserRepository {
    override suspend fun login(email: String, password: String): FireBaseState<String> {
        return when {
            email.isEmpty() -> FireBaseState.Fail("Email must not be empty")
            password.isEmpty() -> FireBaseState.Fail("Password must not be empty")
            !userDataSource.login(
                email,
                password
            ) -> FireBaseState.Fail("Email or password is incorrect")
            else -> FireBaseState.Success("")
        }
    }

    override suspend fun signup(user: User): FireBaseState<String> {
        val userFirebaseModel = UserMapper.mapFromModel(user)
        return when {
            userFirebaseModel.email.isEmpty() -> FireBaseState.Fail("Email must not be empty")
            userFirebaseModel.password.isEmpty() -> FireBaseState.Fail("Password must not be empty")
            !InputFieldValidator.isValidEmail(userFirebaseModel.email) -> FireBaseState.Fail("Email is in valid")
            !InputFieldValidator.isValidPassword(userFirebaseModel.password) -> FireBaseState.Fail("Password is in valid")
            !userDataSource.signup(userFirebaseModel) -> FireBaseState.Fail("This email is already existed")
            else -> FireBaseState.Success("Sign up success")
        }
    }

    override fun getUserInfo(): Flow<User> {
        return userDataSource.getUserInfo().map {
            UserMapper.mapFromFirebaseModel(it)
        }
    }

    override fun logout() {
        userDataSource.logout()
    }

    override suspend fun updateUserProfile(user: User?, updatedInformation: HashMap<String, Any?>): FireBaseState<String> {
        return try {
            if (user != null) {
                userDataSource.updateUserProfile(UserMapper.mapFromModel(user), updatedInformation)
                FireBaseState.Success(null)
            } else {
                FireBaseState.Fail("")
            }
        } catch (ex: FirebaseException) {
            FireBaseState.Fail(ex.message)
        }
    }

    override fun getFavoriteNewsByCurrentUser(): Flow<List<Article>> {
        return favoriteNewsDataSource.getFavoriteNews().map { NewsMapper.mapFromFirebaseModel(it) }
    }

    override suspend fun addToFavorite(article: Article): FireBaseState<String> {
        return if (favoriteNewsDataSource.addToFavorite(NewsMapper.mapFromModelToFirebaseModel(article))) {
            FireBaseState.Success(null)
        } else {
            FireBaseState.Fail("Add failed")
        }
    }

    override suspend fun removeNewsInFavorite(article: Article): FireBaseState<String> {
        return if (favoriteNewsDataSource.removeFavorite(NewsMapper.mapFromModelToFirebaseModel(article))) {
            FireBaseState.Success(null)
        } else {
            FireBaseState.Fail("Remove failed")
        }
    }
}