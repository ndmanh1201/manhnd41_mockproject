package com.example.mockprojectnewsapp.data.repository

import com.example.mockprojectnewsapp.data.local.NewsDao
import com.example.mockprojectnewsapp.data.mapper.NewsMapper
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

class NewsRepositoryLocalImpl @Inject constructor(
    private val newsDao: NewsDao
) : NewsRepository {
    override fun getNewsByCategory(category: String): Flow<List<Article>> {
        return newsDao.getNewsByCategory(category).map {
            NewsMapper.mapFromEntity(it)
        }
    }

    override fun getNewsInTheUS(): Flow<List<Article>> {
        return newsDao.getNewsByCategory("usnews").map {
            NewsMapper.mapFromEntity(it)
        }
    }


    override fun saveToDatabase(articleList: List<Article>, category: String) {
        CoroutineScope(Dispatchers.IO).launch {
            newsDao.insertToDatabase(NewsMapper.mapFromModel(articleList, category))
        }
    }

    override fun searchNews(searchString: String): Flow<List<Article>> {
        return newsDao.searchNews(searchString).map {
            NewsMapper.mapFromEntity(it)
        }
    }

}