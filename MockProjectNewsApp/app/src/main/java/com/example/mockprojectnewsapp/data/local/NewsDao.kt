package com.example.mockprojectnewsapp.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.mockprojectnewsapp.data.model.ArticleEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertToDatabase(articleEntityList: List<ArticleEntity>)

    @Query("SELECT * FROM Article WHERE category = :category")
    fun getNewsByCategory(category: String): Flow<List<ArticleEntity>>

    @Query("SELECT * FROM Article WHERE title LIKE '%' || :searchString || '%' ")
    fun searchNews(searchString: String): Flow<List<ArticleEntity>>

}