package com.example.mockprojectnewsapp.di

import com.example.mockprojectnewsapp.utils.provider.DefaultDispatcher
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DispatcherModule {

    @Provides
    @Singleton
    fun provideDispatcherProvider() : DispatcherProvider {
        return DefaultDispatcher()
    }
}