package com.example.mockprojectnewsapp.domain.model

data class User(
    var email: String = "",
    var password: String = "",
    var username: String = "",
    var firstName: String? = "",
    var lastName: String? = "",
    var dateOfBirth: String? = "",
    var imageUrl: String? = ""
)