package com.example.mockprojectnewsapp.ui.component.detail

import android.content.Intent
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.mockprojectnewsapp.databinding.FragmentDetaillBinding
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import com.example.mockprojectnewsapp.ui.component.favorite.FavoriteNewsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetaillBinding>(FragmentDetaillBinding::inflate) {

    private val args: DetailFragmentArgs by navArgs()

    private val favoriteNewsViewModel: FavoriteNewsViewModel by viewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        favoriteNewsViewModel.addToFavoriteState.observe(viewLifecycleOwner) {
            handleState(it)
        }
    }

    private fun handleState(fireBaseState: FireBaseState<String>?) {
        when(fireBaseState) {
            is FireBaseState.Success -> Toast.makeText(requireContext(), "Added to favorite", Toast.LENGTH_SHORT).show()
            else -> Toast.makeText(requireContext(), "Add to favorite failed", Toast.LENGTH_SHORT).show()
        }
    }

    override fun bindView() {
        super.bindView()
        val article = args.article
        binding.apply {
            Glide.with(root)
                .load(article.urlToImage)
                .into(imgArticleImage)

            tvTitle.text = article.title
            tvAuthor.text = article.author
            tvDescription.text = article.description
            tvContent.text = article.content
            imgBack.setOnClickListener {
                findNavController().popBackStack()
            }
        }

        binding.btnShare.setOnClickListener {
            Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_TEXT, article.urlToImage)
                try {
                    startActivity(Intent.createChooser(this, "Select an action"))
                } catch (ex: Exception) {
                    // (handle error)
                }
            }
        }

        binding.btnAddToFavorite.setOnClickListener {
            favoriteNewsViewModel.addToFavorite(article)
        }
    }

}