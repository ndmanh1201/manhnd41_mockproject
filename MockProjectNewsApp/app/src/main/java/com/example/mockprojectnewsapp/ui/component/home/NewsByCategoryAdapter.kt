package com.example.mockprojectnewsapp.ui.component.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.mockprojectnewsapp.databinding.ItemNewsByCategoryBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.ui.base.BaseAdapter
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import javax.inject.Inject

class NewsByCategoryAdapter @Inject constructor(
    private val imageLoader: ImageLoader
) : BaseAdapter<ItemNewsByCategoryBinding, Article>() {
    override fun bindViewHolder(binding: ItemNewsByCategoryBinding, item: Article) {
        binding.apply {
            item.also {
                tvAuthor.text = it.author
                tvTitle.text = it.title

                imageLoader.load(imgNewsImage, it.urlToImage!!)
            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemNewsByCategoryBinding
        get() = { inflater, parent, _ ->
            ItemNewsByCategoryBinding.inflate(inflater, parent, false)
        }
}