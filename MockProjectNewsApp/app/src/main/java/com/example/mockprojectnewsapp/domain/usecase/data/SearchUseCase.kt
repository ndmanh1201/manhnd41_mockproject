package com.example.mockprojectnewsapp.domain.usecase.data

import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryLocal
import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryNetwork
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SearchUseCase @Inject constructor(
    @NewsRepositoryNetwork private val newsRepositoryNetwork: NewsRepository,
    @NewsRepositoryLocal private val newsRepositoryLocalImpl: NewsRepository
) {
    fun fromNetWork(searchString: String): Flow<List<Article>> = newsRepositoryNetwork.searchNews(searchString)
    fun fromLocal(searchString: String): Flow<List<Article>> = newsRepositoryLocalImpl.searchNews(searchString)
}