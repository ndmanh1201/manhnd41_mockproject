package com.example.mockprojectnewsapp.ui.component.home

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.mockprojectnewsapp.databinding.ItemLatestNewsListBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.ui.base.BaseAdapter
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import javax.inject.Inject

class LatestNewsAdapter @Inject constructor(
    private val imageLoader: ImageLoader
) : BaseAdapter<ItemLatestNewsListBinding, Article>() {

    override fun bindViewHolder(binding: ItemLatestNewsListBinding, item: Article) {
        binding.apply {
            item.also {
                tvLatestNewsAuthor.text = "by ${it.author}"
                tvLatestNewsTitle.text = it.title
                tvLatestNewsDescription.text = it.description

//                Glide.with(this.root)
//                    .load(it.urlToImage)
//                    .into(imgLatestNewsImage)

                imageLoader.load(imgLatestNewsImage, it.urlToImage!!)

            }
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemLatestNewsListBinding
        get() = { inflater, parent, _ ->
            ItemLatestNewsListBinding.inflate(inflater, parent, false)
        }
}