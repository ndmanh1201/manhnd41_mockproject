package com.example.mockprojectnewsapp.utils

import android.content.Context
import android.graphics.drawable.StateListDrawable
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.mockprojectnewsapp.R
import com.example.mockprojectnewsapp.ui.component.viewpager.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

object TabLayoutConfig {

    private val categoryList = listOf(
        "Technology",
        "Sports",
        "Entertainment",
        "Health"
    )

    fun setUpTabLayout(tabLayout: TabLayout, context: Context) {
        val tabCount: Int = tabLayout.tabCount

        for (i in 0 until tabCount) {
            val tabView: View = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tabView.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(0, 0, 30, 0)
            tabView.requestLayout()
            ViewCompat.setBackground(tabView, setImageButtonStateNew(context))
            ViewCompat.setPaddingRelative(
                tabView,
                tabView.paddingStart,
                tabView.paddingTop,
                tabView.paddingEnd,
                tabView.paddingBottom
            )
        }
    }

    private fun setImageButtonStateNew(mContext: Context): StateListDrawable {
        val states = StateListDrawable()
        states.addState(
            intArrayOf(android.R.attr.state_selected),
            ContextCompat.getDrawable(mContext, R.drawable.tab_bg_normal_primary_color)
        )
        states.addState(
            intArrayOf(-android.R.attr.state_selected),
            ContextCompat.getDrawable(mContext, R.drawable.tab_bg_normal)
        )

        return states
    }

    fun setUpTabLayoutWithViewPager(tabLayout: TabLayout, viewPager: ViewPager2) {
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = categoryList[position]
        }.attach()
    }

    fun setUpViewPager(viewPager: ViewPager2, fragment: Fragment) {
        val adapter = ViewPagerAdapter(fragment, 4)
        viewPager.adapter = adapter
    }
}