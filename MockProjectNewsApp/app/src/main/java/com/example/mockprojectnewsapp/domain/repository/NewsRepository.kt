package com.example.mockprojectnewsapp.domain.repository

import com.example.mockprojectnewsapp.domain.model.Article
import kotlinx.coroutines.flow.Flow

interface NewsRepository {
    fun getNewsByCategory(category: String): Flow<List<Article>>

    fun getNewsInTheUS(): Flow<List<Article>>
//    suspend fun getNewsInTheUS(): List<Article>
    fun saveToDatabase(articleList: List<Article>, category: String)
    fun searchNews(searchString: String): Flow<List<Article>>

}