package com.example.mockprojectnewsapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.mockprojectnewsapp.data.model.ArticleEntity

@Database(entities = [ArticleEntity::class], version = 1, exportSchema = false)
abstract class NewsDatabase : RoomDatabase() {
    abstract val newsDao: NewsDao
}