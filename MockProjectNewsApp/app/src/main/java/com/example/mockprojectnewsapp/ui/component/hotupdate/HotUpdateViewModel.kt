package com.example.mockprojectnewsapp.ui.component.hotupdate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsInTheUSUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.SaveToDatabaseUseCase
import com.example.mockprojectnewsapp.utils.NetworkConfig
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class HotUpdateViewModel @Inject constructor(
    private val getNewsInTheUSUseCase: GetNewsInTheUSUseCase,
    private val saveToDatabaseUseCase: SaveToDatabaseUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _latestUSNewsList = MutableLiveData<List<Article>>()
    val latestUSNewsList: LiveData<List<Article>> get() = _latestUSNewsList

    fun getLatestUSNewsList() {
        viewModelScope.launch(dispatcher.io) {
            NetworkConfig.isNetworkConnected.collect { isConnected ->
                if (isConnected) {
                    getLatestUSNewsListFromNetwork()
                } else {
                    getLatestUSNewsFromLocal()
                }
            }
        }
    }

    private fun getLatestUSNewsListFromNetwork() {
        viewModelScope.launch(dispatcher.main) {
            getNewsInTheUSUseCase.fromNetwork().collect {
                saveToDatabaseUseCase(it, "usnews")
            }
            getNewsInTheUSUseCase.fromLocal().collect {
                _latestUSNewsList.value = it
            }
        }
    }

    private fun getLatestUSNewsFromLocal() {
        viewModelScope.launch(dispatcher.main) {
            getNewsInTheUSUseCase.fromLocal().collect {
                _latestUSNewsList.value = it
            }
        }
    }

}