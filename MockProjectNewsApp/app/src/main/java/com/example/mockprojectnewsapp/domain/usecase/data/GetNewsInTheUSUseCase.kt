package com.example.mockprojectnewsapp.domain.usecase.data

import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryNetwork
import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryLocal
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetNewsInTheUSUseCase @Inject constructor(
    @NewsRepositoryNetwork private val newsRepositoryNetwork: NewsRepository,
    @NewsRepositoryLocal private val newsRepositoryLocal: NewsRepository
){
    fun fromNetwork(): Flow<List<Article>> = newsRepositoryNetwork.getNewsInTheUS()
    fun fromLocal(): Flow<List<Article>> = newsRepositoryLocal.getNewsInTheUS()
}