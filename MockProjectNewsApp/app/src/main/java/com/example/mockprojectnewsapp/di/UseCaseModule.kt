package com.example.mockprojectnewsapp.di

import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsByCategoryUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsInTheUSUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.SaveToDatabaseUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Provides
    @Singleton
    fun provideGetNewsByCategoryUseCase(
        @RepositoryModule.NewsRepositoryNetwork newsRepositoryNetwork: NewsRepository,
        @RepositoryModule.NewsRepositoryLocal newsRepositoryLocal: NewsRepository
    ): GetNewsByCategoryUseCase {
        return GetNewsByCategoryUseCase(newsRepositoryNetwork, newsRepositoryLocal)
    }

    @Provides
    @Singleton
    fun provideSaveToDatabaseUseCase(
        @RepositoryModule.NewsRepositoryLocal newsRepositoryLocal: NewsRepository
    ): SaveToDatabaseUseCase {
        return SaveToDatabaseUseCase(newsRepositoryLocal)
    }

    @Provides
    @Singleton
    fun provideGetNewsInTheUSUseCase(
        @RepositoryModule.NewsRepositoryNetwork newsRepositoryNetwork: NewsRepository,
        @RepositoryModule.NewsRepositoryLocal newsRepositoryLocal: NewsRepository
    ): GetNewsInTheUSUseCase {
        return GetNewsInTheUSUseCase(newsRepositoryNetwork, newsRepositoryLocal)
    }

}