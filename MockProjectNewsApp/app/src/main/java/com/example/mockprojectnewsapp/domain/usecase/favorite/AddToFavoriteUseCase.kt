package com.example.mockprojectnewsapp.domain.usecase.favorite

import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.UserRepository
import javax.inject.Inject

class AddToFavoriteUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(article: Article): FireBaseState<String> = userRepository.addToFavorite(article)
}