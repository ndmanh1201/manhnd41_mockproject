package com.example.mockprojectnewsapp.ui.interfaces

import android.view.View

interface IOnItemClickListener {
    fun onItemClick(view: View, position: Int)
}