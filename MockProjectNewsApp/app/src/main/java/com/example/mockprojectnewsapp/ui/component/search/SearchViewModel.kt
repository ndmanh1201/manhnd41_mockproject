package com.example.mockprojectnewsapp.ui.component.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.usecase.data.SearchUseCase
import com.example.mockprojectnewsapp.utils.NetworkConfig
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchUseCase: SearchUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private var _searchResult = MutableLiveData<List<Article>>()
    val searchResult: LiveData<List<Article>> get() = _searchResult

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    fun searchNews(searchFlow: Flow<String>) {
        viewModelScope.launch {
            NetworkConfig.isNetworkConnected.collect { isConnected ->
                searchFlow
                     //wait for user to finish typing, then search -> avoid too many unnecessary APIs call
                    .debounce(300)

                    //filter empty string
                    .filter { queryString ->
                        return@filter queryString.isNotEmpty()
                    }

                    //avoid duplicate network calls
                    .distinctUntilChanged()
                    .flatMapLatest {
                        if (isConnected) {
                            searchUseCase.fromNetWork(it)
                        } else {
                            searchUseCase.fromLocal(it)
                        }
                    }.flowOn(dispatcher.io)
                    .collect {
                        _searchResult.postValue(it)
                    }
            }
        }
    }
}