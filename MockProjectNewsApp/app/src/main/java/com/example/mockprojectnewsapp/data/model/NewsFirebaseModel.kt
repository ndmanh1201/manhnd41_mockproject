package com.example.mockprojectnewsapp.data.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class NewsFirebaseModel(
    var uid: String = "",
    var title: String = "",
    var author: String = "",
    var description: String = "",
    var content: String = "",
    var publishedAt: String = "",
    var urlToImage: String = "",
    var category: String = ""
)