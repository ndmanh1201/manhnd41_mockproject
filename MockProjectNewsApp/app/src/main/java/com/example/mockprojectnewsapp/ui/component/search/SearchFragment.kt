package com.example.mockprojectnewsapp.ui.component.search

import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mockprojectnewsapp.databinding.FragmentSearchBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.utils.extensions.setQueryTextChangeStateFlow
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>(FragmentSearchBinding::inflate) {

    private val searchViewModel: SearchViewModel by viewModels()
    @Inject lateinit var searchAdapter: SearchAdapter

    override fun observeViewModel() {
        super.observeViewModel()
        searchViewModel.searchResult.observe(viewLifecycleOwner) { result ->
            setUpAdapter(result)
        }
    }

    override fun bindView() {
        super.bindView()
        search()

        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun search() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                searchViewModel.searchNews(
                    binding.edtSearch.setQueryTextChangeStateFlow()
                )
            }
        }
    }

    private fun setUpAdapter(list: List<Article>) {
        searchAdapter.submitList(list)
        binding.recyclerViewSearchResult.apply {
            adapter = searchAdapter
            layoutManager = LinearLayoutManager(requireContext())
            searchAdapter.setOnItemClickListener {
                val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment(it)
                findNavController().navigate(action)
            }
        }
    }

}