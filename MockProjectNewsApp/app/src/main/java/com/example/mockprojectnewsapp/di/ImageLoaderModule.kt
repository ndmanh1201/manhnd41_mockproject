package com.example.mockprojectnewsapp.di

import com.example.mockprojectnewsapp.utils.imageloader.GlideImageLoader
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
abstract class ImageLoaderModule {
    @Binds
    abstract fun bindGlideImageLoader(glideImageLoader: GlideImageLoader): ImageLoader
}