package com.example.mockprojectnewsapp.domain.usecase.user

import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.repository.UserRepository
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(user: User) = userRepository.signup(user)
}