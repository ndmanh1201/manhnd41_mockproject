package com.example.mockprojectnewsapp.di

import com.example.mockprojectnewsapp.data.local.NewsDao
import com.example.mockprojectnewsapp.data.network.api.ApiService
import com.example.mockprojectnewsapp.data.network.firebase.FavoriteNewsDataSource
import com.example.mockprojectnewsapp.data.network.firebase.UserDataSource
import com.example.mockprojectnewsapp.data.repository.NewsRepositoryLocalImpl
import com.example.mockprojectnewsapp.data.repository.NewsRepositoryNetworkImpl
import com.example.mockprojectnewsapp.data.repository.UserRepositoryImpl
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import com.example.mockprojectnewsapp.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class NewsRepositoryNetwork

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class NewsRepositoryLocal

    @Provides
    @NewsRepositoryNetwork
    fun provideNewsRepositoryNetwork(apiService: ApiService) : NewsRepository {
        return NewsRepositoryNetworkImpl(apiService)
    }


    @Provides
    @NewsRepositoryLocal
    fun provideNewsRepositoryLocal(newsDao: NewsDao) : NewsRepository {
        return NewsRepositoryLocalImpl(newsDao)
    }

    @Provides
    @Singleton
    fun provideUserRepository(userDataSource: UserDataSource, favoriteNewsDataSource: FavoriteNewsDataSource) : UserRepository {
        return UserRepositoryImpl(userDataSource, favoriteNewsDataSource)
    }

}