package com.example.mockprojectnewsapp.domain.usecase.user

import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.repository.UserRepository
import javax.inject.Inject

class UserProfileUpdateUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(
        user: User?,
        updatedInformation: HashMap<String, Any?>
    ): FireBaseState<String> = userRepository.updateUserProfile(user, updatedInformation)
}