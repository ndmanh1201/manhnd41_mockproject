package com.example.mockprojectnewsapp.ui.component.profile

import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.mockprojectnewsapp.databinding.FragmentEditProfileBinding
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class EditProfileFragment :
    BaseFragment<FragmentEditProfileBinding>(FragmentEditProfileBinding::inflate) {

    @Inject lateinit var imageLoader: ImageLoader

    private val userInformationViewModel: UserInformationViewModel by activityViewModels()
    var imagePicker = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        if (uri != null) {
            binding.imgEditUserImage.setImageURI(uri)
            userInformationViewModel.getUserImage(uri)
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        userInformationViewModel.userInfo.observe(viewLifecycleOwner) {
            updateUI(it)
        }

        lifecycleScope.launch {
            userInformationViewModel.updateProfileState.collect {
                handleState(it)
            }
        }

    }

    private fun handleState(fireBaseState: FireBaseState<String>) {
        when (fireBaseState) {
            is FireBaseState.Success -> Toast.makeText(
                requireContext(),
                "Update success",
                Toast.LENGTH_SHORT
            ).show()
            is FireBaseState.Fail -> Toast.makeText(
                requireContext(),
                "Update fail",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun bindView() {
        super.bindView()
        val user = userInformationViewModel.userInfo.value

        //set image for user
        userInformationViewModel.userImageUri.observe(viewLifecycleOwner) {
            if (it != null) {
                user?.imageUrl = it.toString()
            }
        }

        binding.btnUpdateProfile.setOnClickListener {
            val newUserInformation : HashMap<String, Any?> = hashMapOf(
                "/firstName" to binding.edtFirstName.text.toString(),
                "/lastName" to binding.edtLastName.text.toString(),
            )

            userInformationViewModel.updateUserProfile(user, newUserInformation)
        }

        binding.imgEditUserImage.setOnClickListener {
            imagePicker.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }

    }

    private fun updateUI(user: User?) {
        binding.apply {
            user?.let {
                tvEditUserEmail.text = it.email
                tvEditUsername.text = it.username
                if (it.imageUrl != null) {
                    imageLoader.load(imgEditUserImage, it.imageUrl!!)
                }
                edtFirstName.setText(it.firstName)
                edtLastName.setText(it.lastName)
            }
        }
    }


}