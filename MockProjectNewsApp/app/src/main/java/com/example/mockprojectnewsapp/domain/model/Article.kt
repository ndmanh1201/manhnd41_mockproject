package com.example.mockprojectnewsapp.domain.model

data class Article(
    val title: String?,
    val author: String?,
    val description: String?,
    val content: String?,
    val publishedAt: String?,
    val urlToImage: String?,
) : java.io.Serializable
