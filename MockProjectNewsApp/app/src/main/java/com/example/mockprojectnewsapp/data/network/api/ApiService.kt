package com.example.mockprojectnewsapp.data.network.api

import com.example.mockprojectnewsapp.BuildConfig
import com.example.mockprojectnewsapp.data.model.NewsResponseEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

   @GET("top-headlines")
   suspend fun getTopHeadlinesByCategory(
        @Query("language") article: String = "en",
        @Query("category") category: String
   ) : NewsResponseEntity

    @GET("top-headlines")
    suspend fun getTopHeadlinesInUs(
        @Query("country") article: String = "us",
    ) : NewsResponseEntity


    @GET("everything")
    suspend fun searchNews(
        @Query("q") searchString: String,
        @Query("searchIn") searchIn: String = "title"
        ) : NewsResponseEntity
}