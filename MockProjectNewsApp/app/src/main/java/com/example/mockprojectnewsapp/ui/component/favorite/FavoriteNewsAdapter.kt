package com.example.mockprojectnewsapp.ui.component.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.mockprojectnewsapp.databinding.ItemFavoriteNewsBinding
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.ui.base.BaseAdapter
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import javax.inject.Inject


class FavoriteNewsAdapter @Inject constructor(
    private val imageLoader: ImageLoader
) : BaseAdapter<ItemFavoriteNewsBinding, Article>() {
    override fun bindViewHolder(binding: ItemFavoriteNewsBinding, item: Article) {
        binding.apply {
            tvItemAuthor.text = item.author
            tvItemDescription.text = item.description
            tvItemTitle.text = item.title

            imageLoader.load(imgItemImage, item.urlToImage!!)
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemFavoriteNewsBinding
        get() = { inflater, parent, _ ->
            ItemFavoriteNewsBinding.inflate(inflater, parent, false)
        }
}