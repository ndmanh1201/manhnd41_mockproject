package com.example.mockprojectnewsapp.data.network.firebase

import android.util.Log
import com.example.mockprojectnewsapp.data.model.NewsFirebaseModel
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await

class FavoriteNewsDataSourceImpl(
    private val auth: FirebaseAuth,
    private val databaseReference: DatabaseReference
) : FavoriteNewsDataSource {
    override suspend fun addToFavorite(newsFirebaseModel: NewsFirebaseModel) : Boolean {
        return try {
            val favoriteNewsRef = databaseReference.child("Favorite").child(auth.currentUser!!.uid).push()
            newsFirebaseModel.uid = favoriteNewsRef.key.toString()
            favoriteNewsRef.setValue(newsFirebaseModel).await()
            true
        } catch (ex: FirebaseException) {
            false
        }

    }

    override fun getFavoriteNews(): Flow<List<NewsFirebaseModel>> {
        val favoriteNewsRef = databaseReference.child("Favorite").child(auth.currentUser!!.uid)
        val favoriteNewsList = arrayListOf<NewsFirebaseModel>()
        return callbackFlow {
            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    favoriteNewsList.clear()
                    for (news in snapshot.children) {
                        news.getValue(NewsFirebaseModel::class.java)
                            ?.let { favoriteNewsList.add(it) }
                    }
                    Log.d("manh", "onDataChange at line 42: $favoriteNewsList")
                    trySend(favoriteNewsList)
                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            }

            favoriteNewsRef.addValueEventListener(valueEventListener)
            awaitClose { favoriteNewsRef.removeEventListener(valueEventListener) }
        }
    }

    override suspend fun removeFavorite(newsFirebaseModel: NewsFirebaseModel): Boolean {
        return try {
            val favoriteNewsRef = databaseReference.child("Favorite").child(auth.currentUser!!.uid).child(newsFirebaseModel.uid)
            favoriteNewsRef.removeValue().await()
            true
        } catch (ex: FirebaseException) {
            false
        }
    }


}