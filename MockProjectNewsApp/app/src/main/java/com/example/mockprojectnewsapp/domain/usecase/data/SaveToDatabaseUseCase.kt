package com.example.mockprojectnewsapp.domain.usecase.data

import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryLocal
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import javax.inject.Inject

class SaveToDatabaseUseCase @Inject constructor(
    @NewsRepositoryLocal private val newsRepositoryLocal: NewsRepository
) {
    operator fun invoke(articleList: List<Article>, category: String) = newsRepositoryLocal.saveToDatabase(articleList, category)
}