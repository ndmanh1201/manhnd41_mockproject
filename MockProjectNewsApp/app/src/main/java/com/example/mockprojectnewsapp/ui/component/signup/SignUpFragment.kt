package com.example.mockprojectnewsapp.ui.component.signup

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.mockprojectnewsapp.databinding.FragmentSignUpBinding
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SignUpFragment : BaseFragment<FragmentSignUpBinding>(FragmentSignUpBinding::inflate) {

    private val signUpViewModel: SignUpViewModel by viewModels()

    override fun observeViewModel() {
        super.observeViewModel()
        lifecycleScope.launch {
            signUpViewModel.registerState.collectLatest {
                handleRegisterState(it)
            }
        }
    }

    override fun bindView() {
        super.bindView()
        binding.apply {
            btnSignUp.setOnClickListener {
                signUp()
            }
        }
    }

    private fun signUp() {
        val user = User()
        binding.apply {
            user.apply {
                username = edtSignupUsername.text.toString()
                email = edtSignupEmail.text.toString()
                password = edtSignupPassword.text.toString()
            }
        }
        signUpViewModel.register(user)
    }


    private fun handleRegisterState(fireBaseState: FireBaseState<String>) {
        when (fireBaseState) {
            is FireBaseState.Success -> {
                Toast.makeText(requireContext(), "Sign up success", Toast.LENGTH_SHORT)
                    .show()
                findNavController().popBackStack()
            }

            is FireBaseState.Fail -> {
                Toast.makeText(requireContext(), fireBaseState.msg.toString(), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}