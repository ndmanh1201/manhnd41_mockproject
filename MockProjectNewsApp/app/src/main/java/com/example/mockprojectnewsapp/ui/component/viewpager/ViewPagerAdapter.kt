package com.example.mockprojectnewsapp.ui.component.viewpager

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(fragment: Fragment, private var totalCount: Int) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = totalCount

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> TechnologyFragment()
            1 -> SportsFragment()
            2 -> EntertainmentFragment()
            3 -> HealthFragment()
            else -> TechnologyFragment()
        }
    }
}