package com.example.mockprojectnewsapp.ui.component.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsByCategoryUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.GetNewsInTheUSUseCase
import com.example.mockprojectnewsapp.domain.usecase.data.SaveToDatabaseUseCase
import com.example.mockprojectnewsapp.utils.NetworkConfig
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getNewsInTheUSUseCase: GetNewsInTheUSUseCase,
    private val getNewsByCategoryUseCase: GetNewsByCategoryUseCase,
    private val saveToDatabaseUseCase: SaveToDatabaseUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _latestUSNewsList = MutableLiveData<List<Article>>()
    val latestUSNewsList: LiveData<List<Article>> get() = _latestUSNewsList

    private val _newsByCategoryList = MutableLiveData<List<Article>>()
    val newsByCategoryList: LiveData<List<Article>> get() = _newsByCategoryList

    fun getMainData() {
        viewModelScope.launch(dispatcher.io) {
            val job1 = async { getLatestUSNewsList() }
            val job2 = async { getNewsByCategory("Technology") }

            job1.await()
            job2.await()
        }
    }

    suspend fun getLatestUSNewsList() {
        viewModelScope.launch(dispatcher.io) {
            NetworkConfig.isNetworkConnected.collect { isConnected ->
                if (isConnected) {
                    getLatestUSNewsListFromNetwork()
                } else {
                    getLatestUSNewsFromLocal()
                }
            }
        }
    }

    suspend fun getNewsByCategory(category: String) {
        viewModelScope.launch(dispatcher.io) {
            NetworkConfig.isNetworkConnected.collect { isConnected ->
                if (isConnected) {
                    getNewsByCategoryFromNetwork(category)
                } else {
                    getNewsByCategoryFromLocal(category)
                }
            }
        }
    }

    private suspend fun getLatestUSNewsListFromNetwork() {
        viewModelScope.launch(dispatcher.main) {
            getNewsInTheUSUseCase.fromNetwork().collect {
                saveToDatabaseUseCase(it, "usnews")
            }
            getNewsInTheUSUseCase.fromLocal().collect {
                _latestUSNewsList.value = it
            }
        }
    }

    private fun getLatestUSNewsFromLocal() {
        viewModelScope.launch(dispatcher.main) {
            getNewsInTheUSUseCase.fromLocal().collect {
                _latestUSNewsList.value = it
            }
        }
    }

    private fun getNewsByCategoryFromNetwork(category: String) {
        viewModelScope.launch(dispatcher.main) {
            getNewsByCategoryUseCase.fromNetwork(category).collect {
                saveToDatabaseUseCase(it, category)
            }
            getNewsByCategoryUseCase.fromLocal(category).collect {
                _newsByCategoryList.value = it
            }
        }
    }

    private fun getNewsByCategoryFromLocal(category: String) {
        viewModelScope.launch(dispatcher.main) {
            getNewsByCategoryUseCase.fromLocal(category).collect {
                _newsByCategoryList.value = it
            }
        }
    }


}