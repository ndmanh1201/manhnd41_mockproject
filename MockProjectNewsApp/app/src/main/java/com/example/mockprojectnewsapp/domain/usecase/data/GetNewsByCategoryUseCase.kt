package com.example.mockprojectnewsapp.domain.usecase.data

import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryNetwork
import com.example.mockprojectnewsapp.di.RepositoryModule.NewsRepositoryLocal
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetNewsByCategoryUseCase @Inject constructor(
    @NewsRepositoryNetwork private val newsRepositoryNetwork: NewsRepository,
    @NewsRepositoryLocal private val newsRepositoryLocal: NewsRepository
) {
    fun fromNetwork(category: String): Flow<List<Article>> = newsRepositoryNetwork.getNewsByCategory(category)
    fun fromLocal(category: String): Flow<List<Article>> = newsRepositoryLocal.getNewsByCategory(category)
}