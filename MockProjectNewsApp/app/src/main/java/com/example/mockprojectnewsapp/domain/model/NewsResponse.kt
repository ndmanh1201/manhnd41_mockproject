package com.example.mockprojectnewsapp.domain.model

data class NewsResponse(
    val status: String,
    val totalResults: Int,
    val article: List<Article>,
)

