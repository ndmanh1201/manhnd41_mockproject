package com.example.mockprojectnewsapp.ui.component.hotupdate

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mockprojectnewsapp.databinding.FragmentLatestNewsBinding
import com.example.mockprojectnewsapp.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HotUpdateFragment : BaseFragment<FragmentLatestNewsBinding>(FragmentLatestNewsBinding::inflate) {

    @Inject lateinit var hotUpdatesAdapter: HotUpdatesAdapter
    private val viewModel: HotUpdateViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CoroutineScope(Dispatchers.IO).launch {
            viewModel.getLatestUSNewsList()
        }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        viewModel.latestUSNewsList.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.progressView.isVisible = true
            } else {
                binding.progressView.isVisible = false
                hotUpdatesAdapter.submitList(it)
                setUpAdapter()
            }
        }
    }

    private fun setUpAdapter() {
        binding.recyclerViewHotUpdates.apply {
            adapter = hotUpdatesAdapter
            layoutManager = LinearLayoutManager(requireContext())
            hotUpdatesAdapter.setOnItemClickListener {
                val action = HotUpdateFragmentDirections.actionHotUpdateFragmentToDetailFragment(it)
                findNavController().navigate(action)
            }
        }
    }

    override fun bindView() {
        super.bindView()
        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

}