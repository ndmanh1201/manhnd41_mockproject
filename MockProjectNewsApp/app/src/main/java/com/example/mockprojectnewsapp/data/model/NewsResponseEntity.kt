package com.example.mockprojectnewsapp.data.model

import com.google.gson.annotations.SerializedName

data class NewsResponseEntity(
    @SerializedName("status")
    val status: String,

    @SerializedName("totalResults")
    val totalResults: Int,

    @SerializedName("articles")
    val articleEntities: List<ArticleEntity>,
)