package com.example.mockprojectnewsapp.data.mapper

import com.example.mockprojectnewsapp.data.model.UserFirebaseModel
import com.example.mockprojectnewsapp.domain.model.User

object UserMapper {

    fun mapFromFirebaseModel(firebaseModel: UserFirebaseModel) : User {
        return User(
            username = firebaseModel.username,
            email = firebaseModel.email,
            firstName = firebaseModel.firstName,
            lastName = firebaseModel.lastName,
            dateOfBirth = firebaseModel.dateOfBirth,
            imageUrl = firebaseModel.imageUrl
        )
    }

    fun mapFromModel(user: User) : UserFirebaseModel {
        return UserFirebaseModel(
            username = user.username,
            email = user.email,
            password = user.password,
            firstName = user.firstName?: "",
            lastName = user.lastName?: "",
            dateOfBirth = user.dateOfBirth?: "",
            imageUrl = user.imageUrl?: ""
        )
    }

}