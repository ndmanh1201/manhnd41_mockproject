package com.example.mockprojectnewsapp.data.repository

import com.example.mockprojectnewsapp.data.network.api.ApiService
import com.example.mockprojectnewsapp.data.mapper.NewsMapper
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.repository.NewsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class NewsRepositoryNetworkImpl @Inject constructor(
    private val apiService: ApiService,
) : NewsRepository {
    override fun getNewsByCategory(category: String): Flow<List<Article>> {
        return flow {
            val newsList =
                NewsMapper.mapFromEntity(apiService.getTopHeadlinesByCategory(category = category).articleEntities)
            emit(newsList)
        }.flowOn(Dispatchers.IO)
    }

    override fun getNewsInTheUS(): Flow<List<Article>> {
        return flow {
            val newsList =
                NewsMapper.mapFromEntity(apiService.getTopHeadlinesInUs().articleEntities)
            emit(newsList)
        }.flowOn(Dispatchers.IO)
    }

    override fun saveToDatabase(articleList: List<Article>, category: String) {
        TODO("Not yet implemented")
    }

    override fun searchNews(searchString: String): Flow<List<Article>> {
        return flow {
            val newsResult = NewsMapper.mapFromEntity(apiService.searchNews(searchString).articleEntities)
            emit(newsResult)
        }
    }



}