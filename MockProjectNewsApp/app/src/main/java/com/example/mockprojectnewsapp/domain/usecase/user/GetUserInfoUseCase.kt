package com.example.mockprojectnewsapp.domain.usecase.user

import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.repository.UserRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetUserInfoUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    operator fun invoke(): Flow<User> = userRepository.getUserInfo()
}