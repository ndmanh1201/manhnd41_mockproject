package com.example.mockprojectnewsapp.ui.component.signup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.User
import com.example.mockprojectnewsapp.domain.usecase.user.RegisterUseCase
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val registerUseCase: RegisterUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _registerState = MutableSharedFlow<FireBaseState<String>>()
    val registerState: SharedFlow<FireBaseState<String>> get() = _registerState

    fun register(user: User) {
        viewModelScope.launch(dispatcher.io) {
            _registerState.emit(registerUseCase(user))
        }
    }

}