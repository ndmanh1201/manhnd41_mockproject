package com.example.mockprojectnewsapp.ui.component.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.usecase.user.LoginUseCase
import com.example.mockprojectnewsapp.domain.usecase.user.LogoutUseCase
import com.example.mockprojectnewsapp.utils.provider.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val logoutUseCase: LogoutUseCase,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    private val _loginState = MutableSharedFlow<FireBaseState<String>>()
    val loginState: SharedFlow<FireBaseState<String>> get() = _loginState

    fun login(email: String, password: String) {
        viewModelScope.launch(dispatcher.io) {
            _loginState.emit(loginUseCase(email, password))
        }
    }

    fun logout() {
        viewModelScope.launch(dispatcher.io) {
            _loginState.emit(FireBaseState.Fail(""))
            logoutUseCase()
        }
    }
}