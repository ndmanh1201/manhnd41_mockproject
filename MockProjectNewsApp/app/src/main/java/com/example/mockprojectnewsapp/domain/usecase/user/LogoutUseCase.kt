package com.example.mockprojectnewsapp.domain.usecase.user

import com.example.mockprojectnewsapp.domain.repository.UserRepository
import javax.inject.Inject

class LogoutUseCase @Inject constructor(
    private val userRepository: UserRepository
) {
    operator fun invoke() = userRepository.logout()
}