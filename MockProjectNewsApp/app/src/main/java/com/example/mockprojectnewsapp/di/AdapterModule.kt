package com.example.mockprojectnewsapp.di

import com.example.mockprojectnewsapp.ui.component.favorite.FavoriteNewsAdapter
import com.example.mockprojectnewsapp.ui.component.home.LatestNewsAdapter
import com.example.mockprojectnewsapp.ui.component.home.NewsByCategoryAdapter
import com.example.mockprojectnewsapp.ui.component.hotupdate.HotUpdatesAdapter
import com.example.mockprojectnewsapp.ui.component.search.SearchAdapter
import com.example.mockprojectnewsapp.utils.imageloader.ImageLoader
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
object AdapterModule {

    @Provides
    fun provideLatestNewsAdapter(imageLoader: ImageLoader): LatestNewsAdapter = LatestNewsAdapter(imageLoader)

    @Provides
    fun provideNewsByCategoryAdapter(imageLoader: ImageLoader): NewsByCategoryAdapter = NewsByCategoryAdapter(imageLoader)

    @Provides
    fun provideHotUpdatesAdapter(imageLoader: ImageLoader): HotUpdatesAdapter = HotUpdatesAdapter(imageLoader)

    @Provides
    fun provideFavoriteAdapter(imageLoader: ImageLoader): FavoriteNewsAdapter = FavoriteNewsAdapter(imageLoader)

    @Provides
    fun provideSearchAdapter(imageLoader: ImageLoader): SearchAdapter = SearchAdapter(imageLoader)
}