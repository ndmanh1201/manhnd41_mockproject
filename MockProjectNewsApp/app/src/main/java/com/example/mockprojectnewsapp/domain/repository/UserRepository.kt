package com.example.mockprojectnewsapp.domain.repository

import com.example.mockprojectnewsapp.domain.FireBaseState
import com.example.mockprojectnewsapp.domain.model.Article
import com.example.mockprojectnewsapp.domain.model.User
import kotlinx.coroutines.flow.Flow


interface UserRepository {
    suspend fun login(email: String, password: String): FireBaseState<String>
    suspend fun signup(user: User): FireBaseState<String>
    fun getUserInfo(): Flow<User>
    fun logout()
    suspend fun updateUserProfile(user: User?, updatedInformation: HashMap<String, Any?>): FireBaseState<String>
    fun getFavoriteNewsByCurrentUser(): Flow<List<Article>>
    suspend fun addToFavorite(article: Article): FireBaseState<String>
    suspend fun removeNewsInFavorite(article: Article): FireBaseState<String>
}