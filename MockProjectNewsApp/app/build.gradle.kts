@file:Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.app.news.android.application)
    alias(libs.plugins.android.hilt)
    id("androidx.navigation.safeargs.kotlin")
    id("com.google.gms.google-services")
    kotlin("kapt")
}

android {
    namespace = "com.example.mockprojectnewsapp"
    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")

        }
    }

    kotlinOptions {
        jvmTarget = "17"
    }
}

dependencies {
    implementation(project(":core:data"))

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.firebase.auth.ktx)
    implementation(libs.firebase.auth.core)
    implementation(libs.firebase.database.ktx)
    implementation(libs.firebase.messaging.ktx)
    implementation(libs.firebase.storage.ktx)
    testImplementation(libs.junit)
    androidTestImplementation(libs.junit.ext)
    androidTestImplementation(libs.espresso.core)
    testImplementation(libs.mockito.core)
    testImplementation(libs.mockito.inline)
    testImplementation(libs.arch.core)


    //hilt
    implementation(libs.hilt.core)
    kapt(libs.hilt.compiler)

    //retrofit
    implementation(libs.retrofit.core)
    implementation(libs.gson)
    implementation(libs.gson.converter)

    implementation(libs.okhttp3.logging.interceptor)

    implementation(libs.androidx.lifecycle.livadata)
    implementation(libs.androidx.lifecycle.viewmodel)
    implementation(libs.androidx.fragment)

    //room
    implementation(libs.androidx.room.core)
    implementation(libs.androidx.room.runtime)
    annotationProcessor(libs.androidx.room.compiler)
    kapt(libs.androidx.room.compiler)

    //navigation
    implementation(libs.androidx.navigation)
    implementation(libs.androidx.navigation.ui)
    // Feature module Support
    implementation(libs.androidx.navigation.dynamic.feature)

    // Testing Navigation
    androidTestImplementation(libs.androidx.navigation.testing)


    implementation(libs.services.map)
    implementation(libs.services.location)

    implementation(libs.androidx.recyclerview.core)
    implementation(libs.androidx.recyclerview.selection)

    implementation(libs.kotlin.coroutines)

    implementation(libs.androidx.coordinatorlayout)

    //glide
    implementation(libs.glide.core)
    implementation(libs.glide.okhttp3.integration)
    kapt(libs.glide.compiler)

    // Rounded Image View
    implementation(libs.roundedimageview.core)

    // Circle Image View
    implementation(libs.circleimageview.core)

    testImplementation(libs.turbine)
    testImplementation(libs.truth)
    testImplementation(libs.coroutines.test)

    implementation(libs.firebase.ui.storage)
    testImplementation(libs.mockk)

}
kapt {
    correctErrorTypes = true
}