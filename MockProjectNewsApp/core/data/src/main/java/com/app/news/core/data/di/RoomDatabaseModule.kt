package com.app.news.core.data.di

import android.content.Context
import androidx.room.Room
import com.app.news.core.data.local.database.NewsDAO
import com.app.news.core.data.local.database.NewsRoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomDatabaseModule {
    @Provides
    @Singleton
    fun provideNewsDao(newsDatabase: NewsRoomDatabase) : NewsDAO {
        return newsDatabase.newsDao
    }

    @Provides
    @Singleton
    fun provideNewsDatabase(@ApplicationContext applicationContext: Context): NewsRoomDatabase {
        return Room.databaseBuilder(
            applicationContext,
            NewsRoomDatabase::class.java,
            "NewsDatabase"
        ).build()
    }
}