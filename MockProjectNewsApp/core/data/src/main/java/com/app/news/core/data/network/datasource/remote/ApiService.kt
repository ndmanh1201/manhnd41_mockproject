package com.app.news.core.data.network.datasource.remote

import com.app.news.core.data.network.model.NewsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("top-headlines")
    suspend fun getTopHeadlinesByCategory(
        @Query("apiKey") apiKey: String = "",
        @Query("language") article: String = "en",
        @Query("category") category: String
    ) : NewsResponse

    @GET("top-headlines")
    suspend fun getTopHeadlinesInUs(
        @Query("apiKey") apiKey: String = "",
        @Query("country") article: String = "us",
    ) : NewsResponse


    @GET("everything")
    suspend fun searchNews(
        @Query("q") searchString: String,
        @Query("apiKey") apiKey: String = "",
        @Query("searchIn") searchIn: String = "title"
    ) : NewsResponse
}