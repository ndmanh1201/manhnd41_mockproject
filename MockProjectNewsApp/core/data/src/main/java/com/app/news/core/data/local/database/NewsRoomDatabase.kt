package com.app.news.core.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.news.core.data.local.model.ArticleRoomModel

@Database(entities = [ArticleRoomModel::class], version = 1, exportSchema = false)
abstract class NewsRoomDatabase : RoomDatabase() {
    abstract val newsDao: NewsDAO
}