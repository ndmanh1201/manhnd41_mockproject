package com.app.news.core.data.local.model

data class NewsRoomModel(
    val status: String,

    val totalResults: Int,

    val articleEntities: List<ArticleRoomModel>,
)