package com.app.news.core.data.local.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.news.core.data.local.model.ArticleRoomModel
import kotlinx.coroutines.flow.Flow

@Dao
interface NewsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertToDatabase(articleEntityList: List<ArticleRoomModel>)

    @Query("SELECT * FROM ArticleModel WHERE category = :category")
    fun getNewsByCategory(category: String): Flow<List<ArticleRoomModel>>

    @Query("SELECT * FROM ArticleModel WHERE title LIKE '%' || :searchString || '%' ")
    fun searchNews(searchString: String): Flow<List<ArticleRoomModel>>
}