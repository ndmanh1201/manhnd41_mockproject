package com.app.news.core.data.network.model

import com.google.gson.annotations.SerializedName

data class ArticleResponse(
    @SerializedName("title")
    val title: String?,

    @SerializedName("author")
    val author: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("content")
    val content: String?,

    @SerializedName("publishedAt")
    val publishedAt: String?,

    @SerializedName("urlToImage")
    val urlToImage: String?,
)