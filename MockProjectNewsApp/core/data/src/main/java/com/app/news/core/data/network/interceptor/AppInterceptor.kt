package com.app.news.core.data.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AppInterceptor(private val apiKey: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
        val originalHttp = chain.request().url
        val newUrl = originalHttp.newBuilder().addQueryParameter("apiKey", apiKey).build()
        val newRequest = request.url(newUrl).build()
        return chain.proceed(newRequest)
    }
}