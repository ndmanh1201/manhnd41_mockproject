package com.app.news.core.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "ArticleModel")
data class ArticleRoomModel(
    @PrimaryKey
    @ColumnInfo("title") var title: String,

    @ColumnInfo("author") var author: String?,

    @ColumnInfo("description") var description: String?,

    @ColumnInfo("content") var content: String?,

    @ColumnInfo("publishedAt") var publishedAt: String?,

    @ColumnInfo("urlToImage") var urlToImage: String?,

    @ColumnInfo("category") var category: String?
)