plugins {
    alias(libs.plugins.app.news.android.library)
    alias(libs.plugins.android.hilt)
    kotlin("kapt")
}

android {
    namespace = "com.app.news.core.data"

    kotlinOptions {
        jvmTarget = "17"
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    testImplementation(libs.junit)
    androidTestImplementation(libs.junit.ext)
    androidTestImplementation(libs.espresso.core)

    //hilt
    implementation(libs.hilt.core)
    kapt(libs.hilt.compiler)

    //retrofit
    implementation(libs.retrofit.core)
    implementation(libs.gson)
    implementation(libs.gson.converter)

    implementation(libs.okhttp3.logging.interceptor)

    implementation(libs.androidx.lifecycle.livadata)
    implementation(libs.androidx.lifecycle.viewmodel)
    implementation(libs.androidx.fragment)

    //room
    implementation(libs.androidx.room.core)
    implementation(libs.androidx.room.runtime)
    annotationProcessor(libs.androidx.room.compiler)
    kapt(libs.androidx.room.compiler)

    //firebase
    implementation(libs.firebase.auth.ktx)
    implementation(libs.firebase.auth.core)
    implementation(libs.firebase.database.ktx)
    implementation(libs.firebase.messaging.ktx)
    implementation(libs.firebase.storage.ktx)
}

kapt {
    correctErrorTypes = true
}